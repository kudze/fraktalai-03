# 03 Simetrinių Iteracinių Atvaizdžių Sistemų simuliacija.

Ši programa simiuoja simetines iteracines atvaizdžių sistemas.

## Priklausomybės

1) Java JDK 19 versija
2) Maven

## Paleidimas

1) `mvn clean package` - Sukompiliuoja programa. (Sukompiliuota programa bus target aplanke).
2) `java -jar Fraktalai3-1.0-SNAPSHOT-jar-with-dependencies.jar` - Paleidžia programą.