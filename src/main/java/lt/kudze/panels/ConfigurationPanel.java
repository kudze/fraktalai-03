package lt.kudze.panels;

import lt.kudze.Window;
import lt.kudze.data.config.Configuration;
import lt.kudze.data.config.Transformation;

import javax.sound.midi.SysexMessage;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ConfigurationPanel extends GrayPanel {
    protected MainPanel mainPanel;

    public ConfigurationPanel(ButtonsPanel buttonsPanel) {
        this.mainPanel = buttonsPanel.getMainPanel();

        this.setLayout(new GridLayout(0, 1, 20, 20));
        this.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));

        this.updateContents();
    }

    protected void updateContents() {
        this.removeAll();

        boolean allowDelete = this.getConfiguration().getTransformations().size() != 1;
        for (Transformation transformation : this.getConfiguration().getTransformations())
            this.add(new ConfigurationTransformationPanel(this, transformation, allowDelete));

        this.add(this.makeAddTransformationButton());

        Window window = this.getMainPanel().getConfigurationPanel().getConfigurationWindow();
        if (window != null) {
            this.revalidate();
            this.repaint();
            window.pack();
        }
    }

    protected void resetCanvas() {
        this.getMainPanel().getCanvasPanel().reset();
    }

    protected void addTransformation() {
        this.getConfiguration().addTransformation(new Transformation());
        this.updateContents();
        this.resetCanvas();
    }

    protected JButton makeAddTransformationButton() {
        JButton result = new JButton("Pridėti transformaciją");
        result.addActionListener(new AddTransformationActionListener(this));
        return result;
    }

    public MainPanel getMainPanel() {
        return mainPanel;
    }

    public Configuration getConfiguration() {
        return this.getMainPanel().getConfiguration();
    }
}

record AddTransformationActionListener(ConfigurationPanel configurationPanel) implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        configurationPanel.addTransformation();
    }
}
