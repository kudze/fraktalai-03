package lt.kudze.panels;

import lt.kudze.data.config.Configuration;

import javax.swing.*;

public class MainPanel extends JPanel {
    protected final Configuration configuration;
    protected ButtonsPanel configurationPanel;
    protected CanvasPanel canvasPanel;

    public MainPanel() {
        this(new Configuration());
    }

    public MainPanel(Configuration configuration) {
        this.configuration = configuration;
        this.configurationPanel = new ButtonsPanel(this);
        this.canvasPanel = new CanvasPanel(this.configuration);

        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        this.add(this.configurationPanel);
        this.add(this.canvasPanel);
    }

    public ButtonsPanel getConfigurationPanel() {
        return configurationPanel;
    }

    public CanvasPanel getCanvasPanel() {
        return canvasPanel;
    }

    public Configuration getConfiguration() {
        return configuration;
    }
}
