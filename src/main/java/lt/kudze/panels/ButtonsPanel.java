package lt.kudze.panels;

import lt.kudze.Main;
import lt.kudze.Window;
import lt.kudze.data.config.Configuration;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

public class ButtonsPanel extends GrayPanel {
    public static final String WINDOW_TITLE = Main.WINDOW_TITLE + " | Konfiguracija";

    protected MainPanel mainPanel;
    protected Window configurationWindow = null;

    public ButtonsPanel(MainPanel mainPanel) {
        this.mainPanel = mainPanel;

        this.setLayout(new FlowLayout(FlowLayout.CENTER, 20, 10));

        this.add(this.makeConfigureButton());
        this.add(this.makeIterateButton());
    }

    public void onConfigureClicked() {
        if (this.configurationWindow != null)
            return;

        this.openConfigurationWindow();
    }

    public void onIterateClicked() {
        this.getMainPanel().getCanvasPanel().iterate();
    }

    protected JButton makeConfigureButton() {
        JButton result = new JButton("Konfiguruoti");
        result.addActionListener(new ConfigureActionListener(this));
        return result;
    }

    protected JButton makeIterateButton() {
        JButton result = new JButton("Iteruoti");
        result.addActionListener(new IterateActionListener(this));
        return result;
    }

    protected void openConfigurationWindow() {
        this.configurationWindow = new Window(WINDOW_TITLE, new ConfigurationPanel(this), new ConfigurationWindowListener(this));
    }

    public MainPanel getMainPanel() {
        return mainPanel;
    }

    public Window getConfigurationWindow() {
        return configurationWindow;
    }
}

record ConfigurationWindowListener(ButtonsPanel panel) implements WindowListener {

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {

    }

    @Override
    public void windowClosed(WindowEvent e) {
        panel.configurationWindow = null;
    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}

record ConfigureActionListener(ButtonsPanel panel) implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        panel.onConfigureClicked();
    }
}

record IterateActionListener(ButtonsPanel panel) implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        panel.onIterateClicked();
    }
}

