package lt.kudze.panels;

import lt.kudze.data.Figure;
import lt.kudze.data.Point;
import lt.kudze.data.Rectangle;
import lt.kudze.data.config.Configuration;
import lt.kudze.data.config.Transformation;
import lt.kudze.data.config.TransformationCalculator;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

import java.awt.*;
import java.util.ArrayList;

public class CanvasPanel extends OrangePanel {
    public static final Color SHAPE_COLOR = new Color(0, 0, 0);
    protected ArrayList<Figure> figures;
    protected Figure initialFigure;
    protected Configuration configuration;

    public CanvasPanel(Configuration configuration) {
        this(configuration, Figure.createSquare(), new Dimension(600, 600));
    }

    public CanvasPanel(Configuration configuration, Figure initialFigure, Dimension canvasSize) {
        this.configuration = configuration;
        this.initialFigure = initialFigure;

        this.reset();
        this.setPreferredSize(canvasSize);
    }

    @Override
    protected void paintComponent(Graphics g) {
        int width = this.getWidth();
        int height = this.getHeight();

        g.setColor(this.getBackground());
        g.fillRect(
                0, 0,
                width, height
        );

        //Draw shapes
        g.setColor(SHAPE_COLOR);
        for (Figure figure : this.figures) {
            for (Rectangle rectangle : figure.getRectangles()) {
                rectangle = transformToScreen(rectangle);

                g.fillRect(
                        rectangle.getX(), rectangle.getY(),
                        rectangle.getWidth(), rectangle.getHeight()
                );
            }
        }
    }

    protected Rectangle transformToScreen(Rectangle rectangle) {
        int scaleFactor = this.getWidth();

        RealMatrix scaleMatrix = MatrixUtils.createRealMatrix(new double[][]{
                {scaleFactor, 0},
                {0, -scaleFactor},
        });

        return new Rectangle(
                transform(scaleMatrix, rectangle.getTopLeft(), new Point(), new Point(0, scaleFactor)),
                transform(scaleMatrix, rectangle.getBottomRight(), new Point(), new Point(0, scaleFactor))
        );
    }

    protected Point transform(RealMatrix matrix, Point point) {
        return new Point(matrix.multiply(point.getVector()));
    }

    protected Point transform(RealMatrix matrix, Point point, Point preOffset, Point postOffset) {
        Point result = new Point();

        result.setX(point.getX() + preOffset.getX());
        result.setY(point.getY() + preOffset.getY());

        result = transform(matrix, result);

        result.setX(result.getX() + postOffset.getX());
        result.setY(result.getY() + postOffset.getY());

        return result;
    }

    private Rectangle transform(Rectangle rectangle, RealMatrix matrix, Point preOffset, Point postOffset) {
        return new Rectangle(
                transform(matrix, rectangle.getTopLeft(), preOffset, postOffset),
                transform(matrix, rectangle.getBottomRight(), preOffset, postOffset)
        );
    }

    private Figure transform(Figure figure, RealMatrix matrix, Point preOffset, Point postOffset) {
        ArrayList<Rectangle> rectangles = new ArrayList<>(figure.getRectangles().size());

        for (Rectangle rectangle : figure.getRectangles())
            rectangles.add(transform(rectangle, matrix, preOffset, postOffset));

        return new Figure(rectangles);
    }

    public void iterate() {
        Configuration config = this.getConfiguration();
        ArrayList<Figure> nextFigures = new ArrayList<>(this.figures.size() * config.getTransformations().size());

        for (Transformation transformation : config.getTransformations()) {
            double coeff = transformation.getCoefficient();

            RealMatrix matrix = TransformationCalculator.convertTypeToMatrix(transformation.getType()).scalarMultiply(coeff);
            Point preOffset = TransformationCalculator.getOffsetBeforeMatrixMultiplication(transformation.getType());
            Point postOffset = TransformationCalculator.getOffsetAfterMatrixMultiplication(transformation.getType(), transformation.getQuad(), coeff);

            for (Figure figure : this.figures)
                nextFigures.add(transform(figure, matrix, preOffset, postOffset));
        }

        this.figures = nextFigures;
        this.repaint();
    }

    public void reset() {
        this.figures = new ArrayList<>();
        this.figures.add(this.initialFigure);
        this.repaint();
    }

    public Configuration getConfiguration() {
        return configuration;
    }
}
