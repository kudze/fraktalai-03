package lt.kudze.panels;

import lt.kudze.components.DoubleInputField;
import lt.kudze.components.DoubleInputFieldListener;
import lt.kudze.components.WhileLabel;
import lt.kudze.data.Figure;
import lt.kudze.data.config.Configuration;
import lt.kudze.data.config.RectangleQuad;
import lt.kudze.data.config.Transformation;
import lt.kudze.data.config.TransformationType;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class ConfigurationTransformationPanel extends GrayPanel {
    protected ConfigurationPanel configurationPanel;
    protected Transformation transformation;
    protected Configuration previewConfiguration;
    protected CanvasPanel preview;

    public ConfigurationTransformationPanel(ConfigurationPanel configurationPanel, Transformation transformation, boolean allowDelete) {
        this.configurationPanel = configurationPanel;
        this.transformation = transformation;

        Transformation transformationPreview = transformation.copy();
        transformationPreview.setCoefficient(1);
        transformationPreview.setQuad(RectangleQuad.BOTTOM_LEFT);
        ArrayList<Transformation> previewTransformations = new ArrayList<>();
        previewTransformations.add(transformationPreview);
        this.previewConfiguration = new Configuration(previewTransformations);

        this.preview = new CanvasPanel(
                this.previewConfiguration,
                Figure.createPreviewShape(),
                new Dimension(50, 50)
        );

        this.setLayout(new GridLayout(1, 0, 20, 0));

        this.add(this.makeTransformationTypeSelect());
        this.add(this.makeTransformationQuadSelect());
        this.add(this.makeTranslationCoefficient());

        JPanel previewContainer = new GrayPanel();
        previewContainer.add(this.preview);
        this.add(previewContainer);

        if (allowDelete)
            this.add(this.makeRemoveButton());
    }

    protected void updatePreview() {
        this.previewConfiguration.getTransformations().forEach(
                (Transformation transformation) -> transformation.setType(this.transformation.getType())
        );
        this.preview.reset();
        this.preview.iterate();
    }

    protected JComponent makeTransformationTypeSelect() {
        JComboBox<TransformationType> comboBox = new JComboBox<>(TransformationType.values());
        comboBox.addActionListener(new TransformationTypeSelectActionListener(this));
        comboBox.setSelectedItem(transformation.getType());

        JPanel result = new GrayVerticalPanel();
        result.add(new WhileLabel("Transformacijos tipas:"));
        result.add(comboBox);

        return result;
    }

    protected JComponent makeTransformationQuadSelect() {
        JComboBox<RectangleQuad> comboBox = new JComboBox<>(RectangleQuad.values());
        comboBox.addActionListener(new TransformationQuadSelectActionListener(this));
        comboBox.setSelectedItem(transformation.getQuad());

        JPanel result = new GrayVerticalPanel();
        result.add(new WhileLabel("Ketvirtis:"));
        result.add(comboBox);

        return result;
    }

    protected JComponent makeTranslationCoefficient() {
        DoubleInputField input = new DoubleInputField(this.transformation.getCoefficient());
        input.addListener(new TransformationCoefficientListener(this));

        JPanel result = new GrayVerticalPanel();
        result.add(new WhileLabel("Sąspaudžio koeficientas:"));
        result.add(input);

        return result;
    }

    protected JComponent makeRemoveButton() {
        JButton button = new JButton("Ištrinti");
        button.addActionListener(new TransformationDeleteActionListener(this));
        return button;
    }

    public ConfigurationPanel getConfigurationPanel() {
        return configurationPanel;
    }

    public Transformation getTransformation() {
        return transformation;
    }
}

record TransformationTypeSelectActionListener(ConfigurationTransformationPanel panel) implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        JComboBox<TransformationType> source = (JComboBox<TransformationType>) e.getSource();
        panel.getTransformation().setType((TransformationType) source.getSelectedItem());
        panel.getConfigurationPanel().resetCanvas();
        panel.updatePreview();
    }

}

record TransformationQuadSelectActionListener(ConfigurationTransformationPanel panel) implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        JComboBox<RectangleQuad> source = (JComboBox<RectangleQuad>) e.getSource();
        panel.getTransformation().setQuad((RectangleQuad) source.getSelectedItem());
        panel.getConfigurationPanel().resetCanvas();
    }

}

record TransformationCoefficientListener(ConfigurationTransformationPanel panel) implements DoubleInputFieldListener {

    @Override
    public void update(double value) {
        panel.getTransformation().setCoefficient(value);
        panel.getConfigurationPanel().resetCanvas();
    }

}

record TransformationDeleteActionListener(ConfigurationTransformationPanel panel) implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        panel.getConfigurationPanel().getConfiguration().getTransformations().remove(panel.getTransformation());
        panel.getConfigurationPanel().updateContents();
        panel.getConfigurationPanel().resetCanvas();
    }
}