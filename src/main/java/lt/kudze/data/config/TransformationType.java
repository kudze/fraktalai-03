package lt.kudze.data.config;

public enum TransformationType {
    IDENTITY,
    ROTATE_PI2,
    ROTATE_PI,
    ROTATE_3PI2,
    Y_EQ_MX_S1,
    OX,
    Y_EQ_X,
    OY
}
