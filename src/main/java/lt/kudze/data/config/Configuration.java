package lt.kudze.data.config;

import lt.kudze.data.Figure;
import org.apache.commons.math3.linear.RealMatrix;

import java.util.ArrayList;

public class Configuration {
    private static ArrayList<Transformation> makeDefaultTransformations() {
        ArrayList<Transformation> transformations = new ArrayList<>();
        transformations.add(new Transformation());
        return transformations;
    }

    private final ArrayList<Transformation> transformations;

    public Configuration() {
        this(Configuration.makeDefaultTransformations());
    }

    public Configuration(ArrayList<Transformation> transformations) {
        this.transformations = transformations;
    }

    public void addTransformation(Transformation transformation) {
        this.transformations.add(transformation);
    }

    public ArrayList<Transformation> getTransformations() {
        return transformations;
    }
}
