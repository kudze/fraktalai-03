package lt.kudze.data.config;

public enum RectangleQuad {
    TOP_LEFT,
    TOP_RIGHT,
    BOTTOM_LEFT,
    BOTTOM_RIGHT
}
