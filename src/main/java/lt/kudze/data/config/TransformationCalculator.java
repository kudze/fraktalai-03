package lt.kudze.data.config;

import lt.kudze.data.Point;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

public class TransformationCalculator {

    public static RealMatrix convertTypeToMatrix(TransformationType type) {
        return switch (type) {
            case IDENTITY -> MatrixUtils.createRealMatrix(new double[][]{
                    {1, 0},
                    {0, 1},
            });
            case ROTATE_PI2 -> MatrixUtils.createRealMatrix(new double[][]{
                    {0, -1},
                    {1, 0}
            });
            case ROTATE_PI -> MatrixUtils.createRealMatrix(new double[][]{
                    {-1, 0},
                    {0, -1}
            });
            case ROTATE_3PI2 -> MatrixUtils.createRealMatrix(new double[][]{
                    {0, 1},
                    {-1, 0}
            });
            case Y_EQ_MX_S1 -> MatrixUtils.createRealMatrix(new double[][]{
                    {0, -1},
                    {-1, 0}
            });
            case OX -> MatrixUtils.createRealMatrix(new double[][]{
                    {1, 0},
                    {0, -1},
            });
            case Y_EQ_X -> MatrixUtils.createRealMatrix(new double[][]{
                    {0, 1},
                    {1, 0},
            });
            case OY -> MatrixUtils.createRealMatrix(new double[][]{
                    {-1, 0},
                    {0, 1},
            });
        };
    }

    public static Point getOffsetBeforeMatrixMultiplication(TransformationType transformationType) {
        return switch (transformationType) {
            case Y_EQ_MX_S1 -> new Point(-1, -1);
            case OX -> new Point(0, -1);
            case OY -> new Point(-1, 0);
            default -> new Point();
        };
    }

    public static Point getOffsetAfterMatrixMultiplication(TransformationType transformationType) {
        return switch (transformationType) {
            case ROTATE_PI2 -> new Point(1, 0);
            case ROTATE_PI -> new Point(1, 1);
            case ROTATE_3PI2 -> new Point(0, 1);
            default -> new Point();
        };
    }

    public static Point getOffsetAfterMatrixMultiplication(TransformationType transformationType, RectangleQuad quad, double coeff) {
        Point result = getOffsetAfterMatrixMultiplication(transformationType);

        result.mult(coeff);
        coeff = 1 - coeff;

        if (quad == RectangleQuad.BOTTOM_RIGHT || quad == RectangleQuad.TOP_RIGHT)
            result.setX(result.getX() + coeff);

        if (quad == RectangleQuad.TOP_LEFT || quad == RectangleQuad.TOP_RIGHT)
            result.setY(result.getY() + coeff);

        return result;
    }

}
