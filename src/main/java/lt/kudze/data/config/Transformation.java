package lt.kudze.data.config;

public class Transformation {
    private TransformationType type;
    private RectangleQuad quad;
    private double coefficient;

    public Transformation() {
        this(TransformationType.IDENTITY, 0.5, RectangleQuad.BOTTOM_LEFT);
    }

    public Transformation(TransformationType type, double coefficient, RectangleQuad quad) {
        this.type = type;
        this.coefficient = coefficient;
        this.quad = quad;
    }

    public Transformation copy() {
        return new Transformation(this.getType(), this.getCoefficient(), this.getQuad());
    }

    public TransformationType getType() {
        return type;
    }

    public void setType(TransformationType type) {
        this.type = type;
    }

    public double getCoefficient() {
        return coefficient;
    }

    public void setCoefficient(double coefficient) {
        this.coefficient = coefficient;
    }

    public RectangleQuad getQuad() {
        return quad;
    }

    public void setQuad(RectangleQuad quad) {
        this.quad = quad;
    }
}
