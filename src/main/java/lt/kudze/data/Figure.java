package lt.kudze.data;

import java.util.ArrayList;
import java.util.List;

public class Figure {
    public static Figure createPreviewShape() {
        ArrayList<Rectangle> rectangles = new ArrayList<>();
        rectangles.add(new Rectangle(new Point(0.1f, 0.211f), new Point(0.211f, 0.1f))); //Center of block
        rectangles.add(new Rectangle(new Point(0.211f, 0.211f), new Point(0.322f, 0.1f))); //Shorter end of block
        rectangles.add(new Rectangle(new Point(0.1f, 0.655f), new Point(0.211f, 0.211f))); //Shorter end of block
        return new Figure(rectangles);
    }

    public static Figure createSquare() {
        ArrayList<Rectangle> rectangles = new ArrayList<>();
        rectangles.add(new Rectangle(new Point(0f, 1), new Point(1, 0))); //Center of block
        return new Figure(rectangles);
    }

    public List<Rectangle> rectangles;

    /**
     * @param rectangles - should be mapped between [0,1] coordinates.
     */
    public Figure(List<Rectangle> rectangles) {
        this.rectangles = rectangles;
    }

    public List<Rectangle> getRectangles() {
        return rectangles;
    }
}
