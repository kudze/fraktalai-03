package lt.kudze.data;

public class Rectangle {
    protected Point topLeft;
    protected Point bottomRight;

    public Rectangle(Point topLeft, Point bottomRight) {
        this.topLeft = topLeft;
        this.bottomRight = bottomRight;
    }

    public Point getTopLeft() {
        return topLeft;
    }

    public Point getBottomRight() {
        return bottomRight;
    }

    public int getX() {
        return (int) Math.floor(Math.min(this.topLeft.getX(), this.bottomRight.getX()));
    }

    public int getY() {
        return (int) Math.floor(Math.min(this.topLeft.getY(), this.bottomRight.getY()));
    }

    public int getWidth() {
        return (int) Math.ceil(Math.abs(this.bottomRight.getX() - this.topLeft.getX()));
    }

    public int getHeight() {
        return (int) Math.ceil(Math.abs(this.topLeft.getY() - this.bottomRight.getY()));
    }
}
