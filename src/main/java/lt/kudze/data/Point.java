package lt.kudze.data;

import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

public class Point {
    protected double x;
    protected double y;

    public Point() {
        this(0, 0);
    }

    public Point(RealMatrix vector)
    {
        this(vector.getData());
    }

    public Point(double[][] vector)
    {
        this(vector[0][0], vector[1][0]);
    }

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public void mult(double mult) {
        this.x *= mult;
        this.y *= mult;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public RealMatrix getVector() {
        return MatrixUtils.createRealMatrix(new double[][]{{this.getX()}, {this.getY()}});
    }
}
