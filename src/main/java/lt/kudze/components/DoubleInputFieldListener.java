package lt.kudze.components;

public interface DoubleInputFieldListener {
    void update(double value);
}
