package lt.kudze.components;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;

public class DoubleInputField extends JTextField {

    public DoubleInputField(double value) {
        super(String.valueOf(value));
    }

    public void addListener(DoubleInputFieldListener listener) {
        this.getDocument().addDocumentListener(new DoubleInputFieldDocumentListener(listener, this));
    }

}

class DoubleInputFieldDocumentListener implements DocumentListener {
    protected DoubleInputFieldListener listener;
    protected DoubleInputField field;

    public DoubleInputFieldDocumentListener(DoubleInputFieldListener listener, DoubleInputField field) {
        this.listener = listener;
        this.field = field;
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        update(e);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        update(e);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        update(e);
    }

    protected void update(DocumentEvent e) {
        try {
            String value = e.getDocument().getText(0, e.getDocument().getLength());
            double result = value.isEmpty() ? 0 : Double.parseDouble(value);
            this.listener.update(result);
        } catch (NumberFormatException exception) {
            SwingUtilities.invokeLater(() -> {
                this.field.setText("0.0");
                this.listener.update(0);
            });
        } catch (BadLocationException exception) {
            exception.printStackTrace();
        }
    }
}