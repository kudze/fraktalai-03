package lt.kudze;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowListener;

public class Window extends JFrame {

    public Window(String title, Component rootComponent) {
        this(title, rootComponent, null);
    }

    public Window(String title, Component rootComponent, WindowListener windowListener) {
        super();

        this.add(rootComponent);
        this.setTitle(title);

        if (windowListener == null)
            this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        else {
            this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            this.addWindowListener(windowListener);
        }

        this.pack();
        this.setResizable(false);
        this.setVisible(true);
    }
}
