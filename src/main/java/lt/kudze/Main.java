package lt.kudze;

import lt.kudze.panels.MainPanel;

//b) Praktinė užduotis. Sudaryti universalią programa: simetrinės grupės fraktalams simuliuoti.
public class Main {
    public static final String WINDOW_TITLE = "Karolis Kraujelis | Fraktalai 3";

    public static void main(String[] args) {
        new Window(WINDOW_TITLE, new MainPanel());
    }
}